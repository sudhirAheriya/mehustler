var hustler = angular.module('hustler', [
    'ui.bootstrap',
    'ui-notification'
])

    .run(["$rootScope", "$location", "$http", "$window", function ($rootScope) {
        // $rootScope.loginUser = (loginUser) ? loginUser : '';

    }])
    .config(function (NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 10000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'top'
        });
    })

    .controller('signUpCtrl', function ($scope, $http, $rootScope, Notification) {
        $scope.a = {}


        $scope.submitData = function () {
            $http.post('/signUpUser', $scope.a).then(function (resp) {
                if (resp.data.type) {
                    Notification.success(resp.data.msg);
                    $scope.a = {}
                } else {
                    Notification.error(resp.data.msg);
                }
            })
        }
    })
    .controller('userProfileCtrl', function ($scope, $http, $rootScope, Notification, $uibModal) {
        getUserData()

        function getUserData() {
            $http.get('/getUserData').then(function (resp) {
                if (resp.data) {
                    $scope.a = resp.data
                    if (!$scope.a.DOB)
                        openModal()
                } else {
                    $scope.a = {}
                }
            })
        }

        function openModal() {
            var statusModalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/modal/updateInfo.html',
                controller: 'updateInfoCtrl',
                windowClass: 'lg-dialog',
                backdrop: 'static',
                resolve: {
                    data: {userId: $scope.a._id}
                }
            })
            statusModalInstance.result.then(function (selectedItem) {
                if (selectedItem.msg == 'done') {
                    getUserData()
                }
            }, function () {
                getUserData()
            });
        }
    })

    .controller('updateInfoCtrl', function ($scope, $http, $window, $uibModalInstance, data, Notification, $rootScope) {
        $scope.a = {}
        $scope.a._id = data.userId;
        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        }

        $scope.open = function ($event) {
            $scope.opened = true;
            $scope.showMessage = 1
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            dateDisabled: true,
            maxDate: new Date(2020, 5, 22),
        };
        $scope.format = 'dd-MMMM-yyyy';

        $scope.submitData = function () {
            $http.post('/updateUser', $scope.a).then(function (resp) {
                if (resp.data.type) {
                    Notification.success(resp.data.msg);
                    $uibModalInstance.close({msg: 'done'})
                } else {
                    Notification.error(resp.data.msg);
                }
            })
        }

    })
    .controller('editProfileCtrl', function ($scope, $http, $rootScope, Notification) {
        $scope.a = doc
        if (doc.DOB) {
            $scope.a.DOB = new Date(doc.DOB)
        }
        $scope.open = function ($event) {
            $scope.opened = true;
            $scope.showMessage = 1
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            dateDisabled: true,
            maxDate: new Date(2020, 5, 22),
        };
        $scope.format = 'dd-MMMM-yyyy';
        $scope.editUser = function () {
            $http.post('/updateUser', $scope.a).then(function (resp) {
                if (resp.data.type) {
                    Notification.success(resp.data.msg);
                } else {
                    Notification.error(resp.data.msg);
                }
            })
        }
    })

