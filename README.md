# Easy Node Authentication

Code for the entire scotch.io tutorial series: Complete Guide to Node Authentication

We will be using Passport to authenticate users locally, with Facebook, Twitter, and Google.

#### Upgraded To Express 4.0
This tutorial has been upgraded to use ExpressJS 4.0. See [the commit](https://github.com/scotch-io/easy-node-authentication/commit/020dea057d5a0664caaeb041b18978237528f9a3) for specific changes.

## Instructions

If you would like to download the code and try it for yourself:

1. Clone the repo: `git clone https://sudhirAheriya@bitbucket.org/sudhirAheriya/mehustler.git`
2. In terminal cd /path/to/your/repo
3. Install packages: `npm install`
4. Install packages: `bower install`
5. Launch: `node hustle.js`
6. Visit in your browser at: `http://localhost:8080`

## The Tutorials

- [Getting Started and Local Authentication](http://scotch.io/tutorials/easy-node-authentication-setup-and-local)
- [Facebook](http://scotch.io/tutorials/easy-node-authentication-facebook)
- [Twitter](http://scotch.io/tutorials/easy-node-authentication-twitter)
- [Google](http://scotch.io/tutorials/easy-node-authentication-google)
- [Linking All Accounts Together](http://scotch.io/tutorials/easy-node-authentication-linking-all-accounts-together)
